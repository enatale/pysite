# makesite.py

This is the source code of [my webpage](https://enatale.name), based on 
[makesite.py](https://github.com/sunainapai/makesite) by [Sunaina
Pai](https://github.com/sunainapai).

The license text of the original makesite.py is included below.


## Credits

Thanks to:

  - [Sunaina Pai](https://github.com/sunainapai) for the version of makesite.py
    project I started from. 
  - [Susam Pal](https://github.com/susam) for the initial documentation
    and the initial unit tests.
  - [Keith Gaughan](https://github.com/kgaughan) for an improved
    single-pass rendering of templates.


## License

This is free and open source software. You can use, copy, modify,
merge, publish, distribute, sublicense, and/or sell copies of it,
under the terms of the [MIT License](LICENSE.md).

This software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
express or implied. See the [MIT License](LICENSE.md) for details.

